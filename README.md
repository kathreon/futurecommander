# FutureCommander

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

## License

This is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License.

## MOVED PERMANENTLY TO https://github.com/narfai/futurecommander